# ACTUALIZACIÓN DE UN SERVIDOR OBSOLETO A UNA VERSIÓN CON SOPORTE    
## 
![Lenny](aux/lenny.jpg) ![Squeeze](aux/squeeze.jpg) ![Wheezy](aux/wheezy.png)
![Jessie](aux/Jessie.png) ![Stretch](aux/stretch.jpg) 

## PRESENTACIÓN
En entornos laborales relacionados con el sector TIC es frecuente encontrarnos con 
hardware y software obsoletos.
  
* Riesgos de seguridad  
* Problemas de rendimiento  
* Problemas de compatibilidad   
Siempre es recomendable estar lo más actualizado posible, pero ¿Qué ocurre si nos 
encontramos con un "dinosaurio"?  ![dinosaurio](aux/oldpc.jpg)
¿Lo sustituimos? ¿Lo actualizamos? ¿En qué nos basamos
para tomar la decisión?  

## SITUACIÓN INICIAL. HARDWARE  
Máquina virtual creada con VMWare Player 15
  
* CPU de un núcleo  
* 2GB de RAM  
* 16 GB de disco duro  
* Tarjeta de red configurada en modo NAT  
* S.O. Linux Debian 5 "Lenny"  
* IP 172.16.191.130  

## SITUACIÓN INICIAL. SOFTWARE
* Kernel de 32 bits versión 2.6.26-2-686
* Apache 2.2.9
* PHP 5.2.6-1
* MySQL 5.0.51a (root/jupiter)
* Courier-imap 3.0.8
* Webmin 1.57 (root/jupiter)
* Wordpress 3.4 (admin/jupiter)
* usuarios: root/jupiter y aitor/clscls

## OBJETIVO: DEBIAN 8 "Jessie" 
* Kernel de 64 bits versión 3.16.0-8-amd64
* Apache 2.4
* PHP 5.6.29
* MySQL 5.7/MariaDB 10.0
* Courier-imap 4.15.1
* Webmin 1.910
* Wordpress 5.1

## PRIMEROS PASOS
El primer paso es crear la máquina virtual. Aunque en teoría no es parte
del proyecto, ha consumido una cantidad significativa de tiempo

* Seleccionar software de virtualización
    * Propietario: **VMWare**, VirtualBox
    * Libre: KVM
* Instalar Debian
* Configurar los servicios
    * Apache + Wordpress
    * Courier
    * MySQL

## SOFTWARE DE VIRTUALIZACIÓN
De las tres opciones barajadas inicialmente seleccionamos VMWare por tres motivos:

* Ya había hecho pruebas con él anteriormente
* No pregunta si ésta es de 32 o 64 bits
* *"Permite"* migrar a los otros sistemas.

## PRIMER BACHE. A LOS PCS DE CLASE NO LE GUSTA EL SOFTWARE PROPIETARIO
Al intentar instalar VMWare o VirtualBox en Fedora 27 hay problemas: no
está soportado.
![vmwareko](aux/errorvmware.jpg)   

##
En mi portátil pude arreglarlo siguiendo este procedimiento:

* Instalar los paquetes *kernel-headers, kernel-devel, gcc, glibc-headers y 
elfutils-liblef-devel*
* Instalar VMWare (descargable en la 
[web oficial](https://my.vmware.com/en/web/vmware/free#desktop_end_user_computing/vmware_workstation_player/15_0))
* Como root, entro en */usr/lib/vmware/modules/source*, descomprimo los archivos 
vmnet.tar y vmmon.tar `tar -xvf vmnet.tar && tar -xvf vmmon.tar`
* Compilo los módulos vmmon y vmnet a partir de las fuentes que acabamos de descomprimir
```
make -C vmmon-only
make -C vmnet-only
```
* Creo un directorio para los módulos recién compilados
```
mkdir /lib/modules/$(uname -r)/misc
cp vmmon-only /vmmon.ko /lib/modules/$(uname -r)/misc
cp vmnet-only/vmnet.ko /lib/modules/$(uname -r)/misc
```
* testeamos los módulos con `depmod -a` y reconfiguramos VMWare para que los reconozca
con `vmware-modconfig --console --install-all`

## PRIMER BACHE. LAS VMs CREADAS CON SOFTWARE PROPIETARIO TAMPOCO
La solución anterior no funciona en los pcs de clase. Tras varios dias sin progresos, paso 
a la opción B, instalar KVM y migrar la máquina virtual. 

La instalación de KVM se completa sin problemas, pero no consigo migrar la máquina 
virtual. En teoría basta con usar este comando   
```qemu-img convert -O qcow2 <input vmdk file> <ouput qcow2 file>```   
Pero no consigo que la nueva máquina reconozca el disco duro virtual.
![kvmko](aux/errorkvm.jpg) 

Plan C, compruebo que puedo instalar desde cero la máquina virtual en KVM y 
que el proceso de actualización se puede seguir independientemente del sofware de 
virtualización, y sigo avanzando.

##  CONFIGURANDO EL SERVIDOR 1. INSTALANDO DEBIAN 5
![Lenny](aux/lenny.jpg)  
Descargo el DVD de instalación de "Lenny" de la 
[web oficial](https://cdimage.debian.org/mirror/cdimage/archive/), lo cargo en la 
VM y arranco. La instalación en si no tiene nada de particular, todas 
las opciones predeterminadas, un usuario root y otro normal. Instalamos el servidor web. 
No encontrará los repositorios oficiales para descargar paquetes, ignoro el error alegremente.

## CONFIGURANDO EL SERVIDOR 2. INSTALAR SERVICIOS
Los repositorios de apt de Lenny llevan años fuera de servicio, pero todas (o casi) las versiones 
descatalogadas de Debian conservan un repositorio llamado archivo, desde el que pueden instalar paquetes.
  
En */etc/apt/sources.list*, comentamos todas las lineas que haya y añadimos
```deb http://archive.debian.org/debian lenny main contrib```  
Guardamos los cambios en el archivo y actualizamos la base de datos de apt con el comando   
```apt-get update```  
Instalo los servicios que voy a usar en el servidor   
``` 
apt-get install mysql-server ssh php5 php5-mysql libpam-mysql
apt-get install courier-imap courier-authlib-mysql courier-pop courier-pop-ssl courier-maildrop
apt-get install getmail4 rkhunter sudo gamin 
```  

## CONFIGURANDO EL SERVIDOR 3. WEBMIN?  
![webmin](aux/debian5webminok.jpg)  

##
Es un panel de control gratuito. Permite gestionar el servidor sin necesidad de abrir un terminal, usando 
un entorno web relativamente sencillo.    
Este panel permite entre otras cosas:
  
* Comprobar el hardware instalado  
* Comprobar los servicios instalados, y sus versiones  
* Actualizar e instalar nuevos servicios  
* Administrar y configurar servicios  
* Administrar y configurar el sistema  
* Gestión de backups (Bacula)  
* Monitorización y control de la red (iptables, TCPWrappers, monitorización de tráfico...)  

## CONFIGURANDO EL SERVIDOR 4. WEBMIN!!
La instalación es simple, basta con descargar la versión que queramos de la 
[web Sourceforge](https://sourceforge.net/projects/webadmin/files/webmin/1.570/webmin_1.570_all.deb/download?use_mirror=datapacket)
Si lo intento instalar tal cual, dará un error de dependencias, primero  
``` 
apt-get install libnet-ssleay-perl libauthen-pam-perl libio-pty-perl apt-show-versions
```
Y, ahora si
``` 
dpkg -i webmin_1.57_all.deb
```
La última versión es la 1.91, instalo la 1.57 que salió en la época en que Debian 5 
quedó obsoleto (2012).

## CONFIGURANDO EL SERVIDOR 5. WORDPRESS
![Wordpress](aux/WordPress.png)  

##
Es un gestor de contenidos web muy utilizado en blogs por su sencillez de uso y la gran 
variedad de plugins disponibles. Necesita 

* PHP5  
* Un servidor web (Apache)  
* Una base de datos (MySQL)  
Antes de comenzar la instalación hace falta crear una BDD. Para ello, como root 
```
mysql -p jupiter
create database wordpress;
exit;
```

## CONFIGURANDO EL SERVIDOR 6. WORDPRESS 2
Descargo de la [web oficial](https://es.wordpress.org/download/releases/) la versión 3.4, 
publicada en 2012, la descomprimo en */var/www/wordpress*.
Como esto es de pruebas lo estoy haciendo todo con root, **no es buena idea**  

Ahora ya puedo acceder al asistente de instalación usando un navegador web y la URL  
```172.16.191.130/wordpress/wp-admin/install.php```
Pedirá los datos de la base de datos que he creado antes, y el usuario y contraseña de MySQL.  

## CONFIGURANDO EL SERVIDOR 7. WORDPRESS 3
Es posible que al intentar cargar install.php se produzca un error, para arreglarlo 
Entro en el directorio */var/www/wordpress* y modifico el archivo wp-cofig.php para indicar dónde
está la base de datos de la web. Si no existe el archivo, copio y renombro wp-config-sample.php
````
/** El nombre de tu base de datos de WordPress */
define('DB_NAME', 'wordpress');

/** Tu nombre de usuario de MySQL */
define('DB_USER', 'root');

** Tu contraseña de MySQL */
define('DB_PASSWORD', 'jupiter');
````

Pedirá el host de la base de datos, por defecto asume que Apache y MySQL están en el mismo 
servidor, pero no tiene por qué ser así.
   
El prefijo de tabla es porque algunos alojamientos web 
no dejan crear más que una BDD, si quieres tener más de una web tienes que diferenciar 
las tablas de cada web con un prefijo dentro de la misma BDD.

## CONFIGURANDO EL SERVIDOR 8. WORDPRESS 4
Ya casi estamos, sólo falta meter el título de la web, el login y clave del administrador de 
la web, el correo al que le llegarán los avisos de la web y si queremos o no que se indexe la web
Y ya lo tenemos.  
![setupwordpress](aux/wpsetup.jpg)

## CONFIGURANDO EL SERVIDOR 9. COURIER 1
Escogí instalar Courier por dos motivos:

* Es un MTA con capacidad para Extended SMTP, IMAP, POP3 y webmail que puede transmitir correo 
entre servidores o entre usuario y servidor. Se usa bastante en combinación con Postfix.
* Encontré [este tutorial](http://www.daeglo.com/projects/couriermailserversetup).     
Inicialmente la idea era configurar varias cuentas
y crear un dominio con DNS, pero la falta de tiempo lo ha dejado en una sola cuenta de correo y
algunos restos de configuraciones abandonadas.

## CONFIGURANDO EL SERVIDOR 10. COURIER 2
Con este servicio tuve bastantes problemas, daba errores de destinatario como si me estuviera pidiendo 
credenciales para entregar el mensaje.

A base de cambiar permisos en los directorios y revisar archivos de configuración conseguí que 
funcionara, pero al subir a "Squeeze" fallaba.    
Siguiendo las pautas de configuración de este otro 
[tutorial](https://www.howtoforge.com/perfect-server-debian-squeeze-with-bind-and-courier-ispconfig-3-p5)
se arregló. En las máquinas no corregí la configuración inicial, en la guía de instalación sí.

## CONFIGURANDO EL SERVIDOR 11. RETOQUES FINALES
* **Instalar PHPMyAdmin**: 
Herramienta de gestión web para SGDB MySQL y MariaDB.
``` 
apt-get install php5-gd php5-imap phpmyadmin libapache2-mod-fcgid php-auth php5-mcrypt mcrypt php5-imagick \
    libapache2-mod-suphp libruby
```
* **Instalar un servicio NTP** para mantener el servidor en hora. No es necesario, pero sí recomendable, 
ya que al ser una máquina virtual cada vez que se suspende o apaga el reloj se "congela"
``` 
apt-get install ntp ntpdate
```
* **Instalar un complemento para Vim**. Esto lo pongo en último lugar porque tardé en descubrirlo, 
pero debería ir el primero de la lista, si esto en Debian Vim da muchos problemas.
``` 
apt-get install vim-nox
```

## CONFIGURANDO EL SERVIDOR 12. COMPROBACIONES  
* Entro en `https://172.16.191.130:10000` para comprobar que webmin funciona. Hago algún cambio 
para ver que es funcional.  
![Webmin](aux/debian5webminok.jpg)   

##
* Entro en `http://172.16.191.130/phpmyadmin` para comprobar que la base de datos y PHPMyAdmin funcionan.   
![PHPMyAdmin](aux/debian5phpmyadminok.jpg)  

##
* Entro en `http://proyecto.net/wp-admin` para comprobar que la web funciona. Creo 
una nueva entrada de blog. Si esto funciona ==> PHP y MySQL funcionan correctamente.    
![Wordpress](aux/debian5webok.jpg)  

##
* Desde una consola de la máquina virtual conectamos por telnet al servidor smtp para testear el 
servicio de correo.  
```less /home/aitor/Maildir```  
 ![mail](aux/debian5mailok.jpg)  

## EMPIEZA EL PROYECTO... ¿Y AHORA QUÉ?  
1. Ir a la documentación a ver qué dice sobre actualizar versiones:   
    * A lo loco, directamente de una vez  
    * Paso a paso  
2. ¿Qué tengo y a dónde lo quiero llevar? ==> Camino a seguir.  
3. Anticipar posibles problemas en el camino:  
    * ¿Todo el software tiene version de 32 y 64 bits?    
    * ¿Hay cambios importantes? (Grub-Grub2, sysv a systemd, MySQL a MariaDB...)  
    * ¿Están disponibles todas las versiones de software necesarias? ¡Internet Archive!  
4. Creo una primera versión de la ruta de actualizaciones  
5. **Clono la máquina virtual** y pruebo el procedimiento.   
6. Cuando falle miserablemente, vuelvo al punto 1 y repito los pasos hasta encontrar una ruta viable.  

## A TENER EN CUENTA ANTES DE EMPEZAR A ROMPER COSAS  
* **NUNCA** tocar la máquina "de producción" hasta que el procedimiento esté claro. **Backup**  
* Los cortes de los servicios son inevitables. Si todo va bien, serán breves. Hay que avisar antes de empezar.  
* Si se hacen las cosas remotamente, cuidado con perder la conectividad  
* Los **entornos gráficos son MALOS**, se actualiza desde terminal puro y duro o habrá **problemas**  
* Se actualiza e instala con **apt**, se comprueba y limpia con **aptitude**  
* **[Internet Archive](https://archive.org) es tu amigo.**  
* Las versiones anteriores a "Jessie" no tienen los repositorios normales, usan el "archivo"  
* MySQL debe actualizarse usando el mismo método usado para instalarlo, o da problemas.  

## PROPUESTA DE ACTUALIZACIÓN FINAL 
Versiones beta en la guía de actualización.

0. Actualizamos todos los programas a la última versión disponible para esa versión del S.O.  
1. Actualizamos a Debian 6 "Squeeze" (Kernel, luego udev, luego grub)  
    * Actualizamos aplicaciones excepto MySQL, Webmin y Wordpress (upgrade, dist-upgrade)  
    * Actualizamos MySQL a 5.1  
    * Actualizamos Webmin a la última versión  
2. Comprobamos que todo funciona  
3. Limpiamos restos del sistema anterior  

##
4. Actualizamos a Debian 7 "Wheezy" 32 bits  
    * Actualizamos los paquetes sin dependencias con `apt-get upgrade`  
    * Actualizamos kernel, libc6-dev y udev  
    * Reiniciamos con el nuevo kernel  
    * Actualizamos MySQL a 5.5  
    * Actualizamos las aplicaciones sin dependencias con `apt-get upgrade`  
    * Actualizamos el resto del sistema con `apt-get dist-upgrade` (no wordpress)  
5. Comprobamos que todo funciona  
6. Limpiamos
7. Comprobamos que el sistema adminte 64 bits
8. Añadimos la arquitectura de 64 bits al sistema con `dpkg --add-architecture`

## 
9. Instalamos el sistema de 64 bits
    * añadimos un kernel de 64 bits
    * reiniciamos con el nuevo kernel
    * instalamos manualmente los paquetes de 64 bits, primero dpkg, tar y apt, luego 
    paquetes importantes de sistema, luego librerías y por último los demás.
    * eliminamos los paquetes de 32 bits, reiniciamos, eliminamos la arquitectura de 32 bits
10. Comprobamos que todo funciona
11. Limpiamos

##
12. Actualizamos a Debian 8 "Jessie"
    * actualizamos los paquetes sin dependencias
    * Actualizamos el resto de paquetes
    * Reiniciamos y comprobamos que todo funciona
13. Arreglamos Apache, las webs deben estar dentro de /var/www/html
14. *Migramos a MariaDB*
15. *Actualizamos Wordpress*

## PASO 1. DE LENNY A SQUEEZE (1/2)  
![Lenny](aux/lenny.jpg) ![Squeeze](aux/squeeze.jpg)  
Antes de empezar, revisar la 
[guía de actualización](https://www.debian.org/releases/squeeze/amd64/release-notes/ch-upgrading.en.html#upgrade-preparations)
oficial.

* **Backup**
* Vaciar las colas de correo. [Documentación](https://www.courier-mta.org/queue.html) de 
Courier para hacerlo
* `apt-get update` para asegurar que la base de datos de paquetes está actualizada, aptitude -> g
para asegurar que no hay paquetes bloqueados o a medio instalar.
* Cambiar sources.list y actualizar la base de datos de apt  
``` 
vim /etc/apt/sources.squeeze
deb http://archive.debian.org/debian squeeze main

cp /etc/apt/sources.list /etc/apt/sources.lenny
cp /etc/apt/sources.squeeze /etc/apt/sources.list
apt-get update
```

## PASO 1. DE LENNY A SQUEEZE (2/2)
* Conveniente grabar log con el proceso
* Actualizar paquetes sin dependencias con `apt get upgrade`, luego kernel, luego libc6-dev, luego udev. 
* Reiniciar para cambiar al nuevo kernel
* Cambiar de grub a grub2, reiniciar con grub2 y si no falla hacer permanente el cambio
* Actualizar resto de paquetes con `apt-get dist-upgrade`.
* Comprobar que todo está en orden y limpiar con `aptitude` -> 'g'
![aptitudeOK](aux/debian5aptitude1.jpg)  

##
* Actualizamos webmin desde el propio panel de webmin  
![webminUpdate](aux/debian6webminok.jpg)  
* Actualizamos MySQL con 
```apt-get install mysql-server-5.1```

## PASO 1. DE LENNY A SQUEEZE (3)
* Revisamos que todos los programas importantes están en la versión que les toca (webmin)
    * Entorno gráfico: webmin
    * Desde terminal: `dpkg -l`
* Comprobamos que todos los servicios funcionan (correo, web, BDD, panel)
![mailOK](aux/debian6mailok.jpg)  

##
![webOK](aux/debian6webok2.jpg)  
* Inicialmente la configuración de correo era defectuosa, y al actualizar falla. La arreglamos 
siguiendo [este tutorial](https://www.howtoforge.com/perfect-server-debian-squeeze-with-bind-and-courier-ispconfig-3-p4)

## PUNTOS CONFLICTIVOS (SQUEEZE)
* MySQL debe actualizarse usando el mismo método que se usó para instalarlo
* Al actualizar el kernel nos avisa que se va a cambiar los nombres de algunos dispositivos de 
bloques, si no se está haciendo es recomendable cambiar las configuraciones de sistema para usar ID en vez 
de nombres.  
![cambioID](aux/debian6cambioid.jpg)  

##
* El sistema de estadísticas de Debian 6 no es compatible con el de 5, se perderán los datos.  
![estadistica](aux/debian6estadist.jpg)  

##
* El sistema de arranque pasa de sysv a ser sysv-rc, basado en dependencias (el antiguo carga secuencialmente, 
el nuevo en paralelo). Los scripts que cargan los servicios deben tener cierta estructura, de lo contrario la 
migración a sysv-rc falla. La [solución aquí](https://wiki.debian.org/LSBInitScripts/DependencyBasedBoot)  
![sysv](aux/debian6sysv.jpg)
* Es posible que al actualizar aparezcan conflictos de dependencias. Para forzar un paquete a instalarse 
usamos 
```dpkg -i --force-overwrite xxx.deb```  
Por defecto los paquetes se descargan en */var/cache/apt/archives*  
* Al actualizar aparecerán algunos avisos de que se va a sustituir archivos de configuración. Por norma conservamos 
el viejo si tiene cambios hechos a mano.

## PASO 2. DE SQUEEZE A WHEEZY 32 BITS (1/2)
![Wheezy](aux/wheezy.png)  
El procedimiento es similar a la actualización a Squeeze. Guía de actualización oficial 
[aquí](https://www.debian.org/releases/wheezy/i386/release-notes/ch-upgrading.en.html)

* Backup 
* Avisar antes de empezar
* Asegur que el sistema está actualizado con `apt-get update && apt-get upgrade`
* Eliminar restos de configuración viejos con `rm -f /etc/*.dpkg-old` (cuidado de no perder nada importante)

##
* Comprobar paquetes a medio instalar, bloqueados o sin configurar 
```
dpkg --audit`
aptitude  search "~ahold"
aptitude --> 'g'
```  
* Cambiar sources.list y actualizar la base de datos de apt  
```  
vim /etc/apt/sources.wheezy  
deb http://archive.debian.org/debian wheezy main  

cp /etc/apt/sources.wheezy /etc/apt/sources.list
apt-get update
```

## PASO 2. DE SQUEEZE A WHEEZY 32 BITS (2/2)
* Conveniente grabar log con el proceso
* Actualizar paquetes sin dependencias con 
```apt get upgrade```
* Actualizar kernel, luego libc6-dev, luego udev. Para averiguar qué kernel usar: 
```grep -q '^flags.*\bpae\b' /proc/cpuinfo && echo "yes" || echo "no"```     
    * Si devuelve "yes" debemos instalar el kernel linux-image-3.2.0-4-686
    * Si devuelve "no" debemos instalar el kernel linux-image-3.2.0-4-486  
* Ojo a los cambios en archivos de configuración  
* Reiniciar con el nuevo kernel (`uname -r`)
* Instalar MySQL 5.5 (`apt-get install mysql-server-5.5`)
* Actualizar el resto del sistema con 
```apt-get dist-upgrade```
* Limpiar
 ```aptitude```-->'g'

##
* Comprobar que todo funciona  
![mailOK](aux/debian7mailok32bit.jpg)    

##
![webOK](aux/debian7webok32bit.jpg)
  
## PUNTOS CONFLICTIVOS (WHEEZY)
* El sistema de estadísticas no es compatible, los datos viejos se perderán
* El directorio de correo cambia de /home/xxx a /var/mail/xxx. Se puede concatenar los archivos para
conservar los correos viejos.
* Es posible que aparezca un error en /etc/apache2/apache2.conf debido a que el archivo httpd.conf ha cambiado de 
ubicación. Ahora está en /etc/apache2/conf.d/httpd.conf  

## PASO 3. DE 32 A 64 BITS (CROSSGRADING) (1/)
Este paso puede llevarse a cabo a partir de Debian 7, y es **delicado**. Hay muchas posibilidades de que el 
sistema quede inutilizado si se reinicia a destiempo o se instala un paquete importante en el orden equivocado.
**Haz copia de seguridad, y luego haz otra más por si acaso**.
Guías de crossgrading [aquí](https://wiki.debian.org/CrossGrading) y 
[aquí](https://anarc.at/services/upgrades/cross-architecture/)    
* Comprobar que el sistema admite los 64 bits 
```
lscpu|grep op-mode
CPU op-mode(s):      32-bit, 64-bit
```  
* Comprobar que estás funcionando en 32 bits con 
```
dpkg --print-architecture
i386
```
* Añadir la arquitectura de 64 bits con `dpkg --add-arquitecture amd64` y comprobar con  
```
dpkg --print-foreign-architecture`  
amd64
```

## PASO 3. DE 32 A 64 BITS (2/)
* `apt-get update`para actualizar la BDD de apt con los paquetes de 64 bits.
* Instalar un kernel de 64 bits con `apt-get install linux-image-amd64` y reiniciar con el 
nuevo kernel. Nos aseguramos de que estamos usando el nuevo kernel con `uname -r`  
* Limpiar el directorio de descargas con `apt-get clean` y actualizar con `apt-get upgrade`.
No debería instalarse nada, ya estamos actualizados.
* Descargar **SIN INSTALAR** las versiones de 64 bits de apt, dpkg y tar, necesarias para instalar 
todo lo demás en el sistema.
``` 
apt-get -d install dpkg:amd64 tar:amd64 apt:amd64
dpkg --install /var/cache/apt/archives/*_amd64.deb  
```   
El orden es importante, si da error volvemos a ejecutar el comando para resolver las dependencias.
* Comprobar que la arquitectura principal ya es 64 bits 
```dpkg --print-architecture```

## PASO 3. DE 32 A 64 BITS (3/)
* Ahora toca descargar e instalar manualmente todas y cada una de las versiones en 64 bits de los 
paquetes del sistema. 
    * Primero los paquetes básicos: base-files, libc6, libgcc1 dash findutils
    * sacar una lista de todo lo que hay que sustituir con `dpkg -l|grep i386>listado32bits.txt`
    * hacer un `grep lib` del listado anterior para sacar los nombres de las librerías, descargar con apt e 
    instalar con dpkg.  
    Algunos darán errores de dependencias que habrá que resolver, otros no tendrán equivalente en 64 bits.
    Esto último puede ser porque el paquete pertenece a un programa que ha sido abandonado o porque hay una 
    versión más moderna del paquete con otro nombre (normalmente muy parecido y con la misma descripción en 
    dpkg)  
    * Usar `grep -v lib` del listado de paquetes para ver lo que falta por instalar, descargar con
    apt-get download e instalar con dpkg -i igual que antes. **Recomendable instalar primero los paquetes 
    relacionados con Perl, Python y PHP**, muchos otros los tienen como dependencia.
* Hacer una última revisión con dpkg -l para asegurar que no queda ningún paquete de 32 sin su equivalente de 
64 bits. En la web de anarc.at hay un script para automatizar la tarea.

## PASO 3. DE 32 A 64 BITS (4/)
* Eliminar todos los paquetes de 32 bits.
``` 
apt-get dist-upgrade -f
apt-get autoremove
aptitude markauto '~ri386~i' 
aptitude purge '~ri386~i'
```  
Es posible que el último comando intente eliminar el kernel que estamos usando, si es así nos pedirá confirmación 
y se la **negaremos**. Volveremos a instalar el kernel de 64 bits con el mismo comando que al principio,
dará un error (no puede borrar el kernel en uso) pero está bien. Reiniciamos, arrancamos con 
el kernel de 64 bits recién instalado y ahora sí podemos borrar el kernel de 32 bits.
* Eliminar la arquitectura de 32 bits con 
```
dpkg --remove-architecture i386
```

## PASO 3. DE 32 A 64 BITS (5/5)
* Comprobar que todo funciona.  
![mailOK](aux/debian7mailok64bit.jpg)    

##
![webOK](aux/debian7webok64bit.jpg)  

## PUNTOS CONFLICTIVOS (64 BITS)
* No reiniciar si no se indica expresamente, o con toda seguridad el sistema quedará inutilizado.
* Es posible que algún paquete pida dependencias raras que no se encuentran en los repositorios, 
apuntarlo y seguir adelante. Probáblemente más adelante se instalará otro paquete que arreglará el conflicto.
* Los nombres de los paquetes y sus descripciones en `dpkg -l`dan pistas:
    * Nombres parecidos indican relación: si el paquete se llama `libapache`, probáblemente necesita Apache (o viceversa).
    * Dos paquetes con distinto nombre y la misma descripción, probáblemente hacen lo mismo. Quédate con el más 
    moderno (o con el que esté en el repositorio) y borra el otro.
* Este proceso es largo (3-5 horas), asegurate de que tienes tiempo para hacerlo de un tirón, porque si lo dejas a medias 
es posible que te toque empezar de cero cuando intentes retomarlo.
* Ojo a la diferencia entre `apt-get download` y `apt-get install -d`

## PASO 4. DE WHEEZY A JESSIE
![Jessie](aux/Jessie.png)  
El procedimiento es similar a la actualización a Wheezy. Guía de actualización oficial 
[aquí](https://www.debian.org/releases/jessie/i386/release-notes/ch-upgrading.es.html)  
* Backup 
* Avisar antes de empezar
* Comprobar que no haya paquetes bloqueados, mal instalados o sin configurar con 
```
aptitude->'g' 
dpkg --audit
dpkg --get-selections "*"
aptitude search "~ahold"
```

##
* Modificar sources.list para que apunte a los repositorios de Jessie (los normales, no archivo)
``` 
vim /etc/apt/sources.jessie
    deb http://deb.debian.org/debian jessie main contrib non-free
    deb-src http://deb.debian.org/debian jessie main contrib non-free
    deb http://deb.debian.org/debian-security/ jessie/updates main contrib non-free
    deb-src http://deb.debian.org/debian-security/ jessie/updates main contrib non-free
    deb http://deb.debian.org/debian jessie-updates main contrib non-free
    deb-src http://deb.debian.org/debian jessie-updates main contrib non-free
cp /etc/apt/sources.jessie /etc/apt/sources.list
apt-get update
```
* Recomendable grabar un log con el proceso
* Instalar los paquetes que no tengan dependencias con ```apt-get upgrade```
* Actualizar el resto de paquetes con ```apt-get dist-upgrade```
* Recomendable eliminar los archivos de configuración antiguos. 
```apt-get purge $(dpkg -l | awk '/^rc/ { print $2 }')```
Si no queremos perder configuraciones que vayamos a necesitar, las copiamos a lugar seguro  
* Reiniciar

##
* Comprobar que todo funciona  
![mailOK](aux/debian8mailok.jpg)  

##
![webOK](aux/debian8webok.jpg)  

##
![webminOK](aux/finalwebmin.jpg)  

## PUNTOS CONFLICTIVOS (JESSIE)
* Es probable que la web haya dejado de funcionar, pero Apache funciona correctamente. Se debe a que 
el directorio de publicación ha pasado a ser /var/www/html. Hay que mover el directorio que contiene las 
webs dentro de este directorio.
* Al actualizar Apache se introducen cambios importantes en las configuraciones, recomendable echar 
un vistazo a [esta web](https://httpd.apache.org/docs/2.4/es/upgrading.html) para arreglar los problemas
que hayan surgido.
* Es posible que actualizar Apache a la versión 2.4 cause problemas con las webs alojadas en el servidor.
Una opción es bloquear la actualización para que se quede en la 2.2. Ojo, también puede dar problemas.
* Cambia el sistema de arranque SysV a Systemd, si algo falla 
[aquí](https://www.debian.org/releases/jessie/i386/release-notes/ch-information.es.html#systemd-upgrade-default-init-system)
hay ayuda.

## ¿QUE FALTA?
* MySQL ha subido de la versión 5.0 a la 5.5. Dos opciones:
    * migrar a MariaDB. Instrucciones 
    [aquí](https://mariadb.com/resources/blog/how-to-migrate-from-mysql-to-mariadb-on-linux-in-five-steps/) 
    En teoría es trivial, son versiones muy parecidas.
    * Seguir con MySQL. Hay que instalar un repositorio de apt nuevo. 
    [Instrucciones](https://dev.mysql.com/doc/mysql-apt-repo-quick-guide/en/)
* WordPress sigue en la versión 3.4. En teoría es muy fácil, incluso es posible actualizar de golpe hasta la
última versión desde el panel de administrador de la web (necesita un servidor ftp en el servidor).
Es más probable que todo funcione correctamente si se hace poco a poco.  
Según la [guía de actualización](https://wordpress.org/support/article/updating-wordpress/) lo recomendado es 
subir a la más reciente saltando versiones de dos en dos (3.4-->3.6-->3.8...).  
La actualización manual consiste básicamente en sustituir un par de directorios y varios archivos por otros nuevos.
**OJO!**, este proceso puede revelar problemas de incompatibilidad de las webs con las nuevas versiones de 
PHP. => multiphp o parchear las webs.      

## CONCLUSIONES
Para alguien que tiene que empezar de cero, acometer esta tarea puede suponer más de 100 horas de búsqueda de 
documentación y otras tantas de testeo, sin garantías de que al final el procedimiento sea viable.  
* Problemas al pasar a 64 bits
* Uso de software que ya no está soportado
* Problemas graves de dependencias de los paquetes
* Configuraciones defectuosas que fallan al actualizar, o que cambian y hay que revisar
Por todo ello, antes siquiera de plantear la opción hay que pensar: 
* ¿Cuánto me costaría configurar un nuevo servidor actualizado?
* ¿Cómo de complicado es migrar la información del servidor viejo al nuevo?
si la respuesta es menor, igual o incluso algo mayor en horas, la migración es preferible, ya 
que el éxito está prácticamente asegurado.  
* El cambio de versión es más sencillo cuanto más reciente

## FIN
Gracias por la atención
 
      