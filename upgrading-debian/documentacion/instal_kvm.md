### Instal·lació de kvm a un GNU/Linux (Fedora)

###### Requeriments previs

Abans d'instal·lar al nostre ordinador qualsevol programari de virtualització
hauríem de comprovar si el nostre maquinari (*hardware*) ens ho permet. No tots
el pc's admeten virtualització per KVM.

Alerta al cas d'Intel: VT (Virtualization Technology) és un terme que acull la
virtualització en sentit general i VT-x era una de les solucions , com VT-d és
una altra més moderna i que no és incompatible amb l'anterior (ni tampoc
		necessària). Per tant, podem tenir alguna BIOS amb el bit de VT activat
i el de VT-d desactivat i podrem virtualitzar (encara que sigui amb menys
		*features*). [Més info en aquest
enllaç](https://software.intel.com/en-us/blogs/2009/06/25/understanding-vt-d-intel-virtualization-technology-for-directed-io/)

Comprovem si la cpu té activades certes banderes: *vmx* (*intel*) o *svm*
(*amd*). Per fer això executem (no cal ser *root*):

```
egrep '^flags.*(vmx|svm)' /proc/cpuinfo
```

Si la consola mostra alguna cosa, podrem *virtualitzar* amb *KVM*, si no surt
res no podrem.

D'altra banda necessitarem tenir el medi que conté el sistema operatiu que
volem instal·lar virtualment, ja sigui una imatge *iso*, un dvd, una *url* ...

##### Instal·lació 

Ara sí com a root, instal·lem els grup de paquets de virtualització:

```
dnf install @virtualization
```

Iniciem el dimoni `libvirtd`:

```
systemctl start libvirtd
```

(Recordem que l'ordre anterior és temporal si necessitem que sigui permanent,
 és a dir a l'engegar la nostra màquina hauríem de substituir *start* per
 *enable*)

Comprovem que els mòduls del *kernel* per a *KVM* s'han carregat:

```
lsmod | grep kvm
```

Si es mostra informació on surt o *kvm_amd* o *kvm_intel* anem bé. En cas
contrari haurem d'anar a la BIOS a activar VT.

Es recomanable que controlem l'espai que ens consumiran les diferents imatges
virtuals, ja que per defecte el directori emprat pot ser un que no s'adapti al
nostre disseny d'espai d'emmagatzemmatge.

[Canvi de directori a on es guarden les
imatges](https://ask.fedoraproject.org/en/question/9584/while-using-libvirt-how-can-i-use-a-location-other-than-varliblibvirtimages-to-store-my-images/)

##### Execució

Executem un dels programes gràfics que ens permet gestionar les màquines
virtuals: 

```
virt-manager
```

L'ordre anterior, s'ha executat des d'una consola però també es pot executar
des de l'escriptori

Un cop establim una connexió ja podrem construir una màquina virtual des de
diferents tipus d'instal·lació o fins i tot utilitzant una imatge que ja
tinguem feta (última opció a la captura de pantalla)

![Pas 1 en la creació d'una màquina virtual](virt-manager-pas1.png)


