## CONTENIDO DEL DIRECTORIO

- Especificacions Mòdul de Projecte ASIX.pdf : archivo con los requisitos a cumplir por el proyecto
- MySQL Reference Manual: manuales de actualización para MySQL
- aptitude.txt: guía rápida de comandos para aptitude
- compatibilidadversiones.xlsx: hoja de cálculo con las versiones más recientes de varios programas que soporta cada S.O.
- instal_kvm.md : guía sobre el software de virtualización KVM, extraída de los apuntes del módulo M01 DE 1º de ASIX, en la E.D.T.
- poster.pdf: poster del proyecto
- proyectoAitorGalilea.mp4: video de presentación del proyecto
