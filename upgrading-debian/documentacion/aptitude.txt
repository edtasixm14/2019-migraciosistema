When it starts, aptitude shows a list of packages sorted by state (installed, non-installed, or installed but not available on the mirrors � other sections display tasks, virtual packages, and new packages that appeared recently on mirrors). To facilitate thematic browsing, other views are available. In all cases, aptitude displays a list combining categories and packages on the screen. Categories are organized through a tree structure, whose  

Enter, [ and ] keys. branches can respectively be unfolded or closed with the
* + should be used to mark a package for installation, 
* - to mark it for removal and _ to purge it (note than these keys can also be used for categories, in which case the corresponding actions will be applied to all the packages of the category). 
* u updates the lists of available packages and 
* Shift+u prepares a global system upgrade. 
* g switches to a summary view of the requested changes (and typing g again will apply the changes), and 
* q quits the current view. If you are in the initial view, this will effectively close aptitude. 
* / To search for a package, you can type / followed by a search pattern. This pattern matches the name of the package, but can also be applied to the description (if preceded by ~d), to the section (with ~s) or to other characteristics detailed in the documentation. The same patterns can filter the list of displayed packages: type the l key (as in limit) and enter the pattern. 